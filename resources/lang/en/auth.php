<?php

return array (
  'failed' => 'The credentials do not match our records. test',
  'password' => 'The provided password is incorrect.',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
);
