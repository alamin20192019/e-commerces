require('./bootstrap');

require('moment');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
import NProgress from 'nprogress'
import { Inertia } from '@inertiajs/inertia';
import Lang from 'lang.js';
import VueLazyload from 'vue-lazyload';
import moment from 'moment'

Vue.prototype.$moment = moment;
Vue.use(VueLazyload);
Inertia.on('start', () => NProgress.start());
Inertia.on('finish', (event) => {
    // return false;
    // console.log(event);
    if(null !== event.detail){
        if (event.detail.visit.completed) {
            NProgress.done();
        } else if (event.detail.visit.interrupted) {
            NProgress.set(0);
        } else if (event.detail.visit.cancelled) {
            NProgress.done();
            NProgress.remove();
        }
    }else{
        NProgress.done();
        // return false;
    }

    
})

Vue.component('v-icon', Icon)
import 'vue-select/dist/vue-select.css';

Vue.mixin({ methods: { route } });
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);

const app = document.getElementById('app');
const pageData = JSON.parse(app.dataset.page);
// console.log(pageData.props.lang);
const default_locale = pageData.props.lang;
const fallback_locale = pageData.props.fallbackLocale;
const messages = pageData.props.messages;

Vue.prototype.trans = new Lang({ messages, locale: default_locale, fallback: fallback_locale });

import "v2-datepicker/lib/index.css"; // v2 need to improt css
import V2Datepicker from "v2-datepicker";

Vue.use(V2Datepicker);

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);
