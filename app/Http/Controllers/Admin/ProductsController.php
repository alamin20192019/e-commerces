<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use SecTheater\Marketplace\Facades\ProductRepository as Product;
use SecTheater\Marketplace\Facades\ProductVariationRepository as Variation;
use SecTheater\Marketplace\Facades\ProductVariationTypeRepository as ProductType;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->has('provider')){
            return abort('403', __('You do not have access to this page'));
        }

        $provider = $request->provider;
        $admins = User::role(['admin'])->get()->pluck('id')->all();
        $vendors = User::role(['vendor'])->get()->pluck('id')->all();
        // dd($admins);

        switch ($provider) {
            case 'admin':
                $items = Product::whereIn('user_id', $admins)
                ->with(['brand', 'category.category'])
                ->get();
                break;
            
            case 'vendor':
                $items = Product::whereNotIn('user_id', $admins)->whereIn('user_id', $vendors)
                ->with(['brand', 'category.category'])
                ->get();
                break;
            
            default:
                $items = Product::whereIn('user_id', $admins)->get();
                break;
        }

        // dd($items);
        return Inertia::render('Admin/Products', [
            'products' => $items,
            'provider' => $provider,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(getCountryISO('HKG'));
        return Inertia::render('Admin/Forms/Product', [
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Product'),
            'countries' => countryList(),
            'cats' => makeParentCategories(true),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Products $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $product)
    {
        //
    }
}
