<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vendor\Vendor;
use App\Models\Vendor\VendorInformation;
use App\Models\Vendor\VendorOtherInformation;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


 
         $email = User::where('email',$request->registrant_email)->first();

         if (isset($email->email) && hasRole('vendor')) {

            return back()->with([
                'flash' => __('You already a vendor! A user can only has 1 vendor account!'),
            ]);
         }





        $type = 'vendor';
        switch ($type) {
            case 'vendor':
                $component = 'Auth/VendorSignUp';
                $title = __('Vendor Signup');
                $page = 'vendor-signup';
                break;
            }

        Validator::make($request->all(), [
            'company_name_en'     => 'required|max:100',
            'company_name_ch'     => 'required|max:100',
            // 'company_name_hk'     => 'required|max:100',
            'incorporation_place' => 'required|string|max:255',
            'incorporation_no'    => 'string|required',
            'business_name'       => 'required|max:100',
            'componay_website'    => 'required|max:100',
            'floor'               => 'required|max:20',
            'street'              => 'required|max:100',
            'district'            => 'required|max:100',
            'country'             => 'required|max:100',
            'business_registation_no' =>'unique:vendors|required|max:120',
            'product_qty'         => 'required|max:100',
            'br_date'             =>  'required',
            'online_shop_status'  => 'required | max:2',
            //'status'              => 'required | max : 10',
            'contact_name'        => 'required|max:100',
            'contact_person_position' => 'required|max:100',
         //  'mobileno_register'        => 'required|integer|max:12',
            'contact_email'           => 'unique:vendor_information| required|email|required',
            'mobileno_contact'         => 'unique:vendor_information| required|integer|required',
            'registrant_name'         => 'required|max:100',
            'registrant_posotion'      => 'required|max:100',
            'registrant_contact_number'=> 'unique:vendor_other_information|required |integer',
            'registrant_email'       => 'unique:vendor_other_information|email|required|max:100',
            'countrycode_contact'     =>  'required',
        ],
        )->validate();
        try {

            $newVendor= new Vendor;
            $newVendor->company_name_en  = $request->company_name_en;
            $newVendor->company_name_ch = $request->company_name_ch;
            // $newVendor->company_name_hk   = $request->company_name_hk;
            $newVendor->incorporation_palce = $request->incorporation_place;
            $newVendor->incorporation_no = $request->incorporation_no;
            $newVendor->business_name = $request->business_name;
            $newVendor->componay_website = $request->componay_website;
            $newVendor->floor    = $request->floor;
            $newVendor->country  = $request->country;
            $newVendor->district = $request->district;
            $newVendor->business_registation_no = $request->business_registation_no;
            $newVendor->product_qty = $request->product_qty;
            $newVendor->br_date     =  date('Y-m-d', strtotime($request->br_date)) ;
            $newVendor->street = $request->street;
            $newVendor->online_shop_status = $request->online_shop_status;
            $newVendor->status = "1";
            $newVendor->save();

            $newVendor->addAllMediaFromTokens($request->input('tokens', []), 'br_file');
            $newVendor->addAllMediaFromTokens($request->input('tokens', []), 'componay_photo');
            $vendor_id=$newVendor->id;


            // $vendor_update = Vendor::find($vendor_id);
            // $vendor_update->temp_vendor_user = $request->all();
            // $vendor_update->save();

            $newvendorInformation = new VendorInformation;
            $newvendorInformation->vendor_id =$vendor_id;
            $newvendorInformation->contact_name = $request->contact_name;
            $newvendorInformation->contact_person_position = $request->contact_person_position;
            $newvendorInformation->mobileno_contact = $request->mobileno_contact;
            $newvendorInformation->country_code = $request->countrycode_contact;
            $newvendorInformation->contact_email = $request->contact_email;

            $newvendorInformation->save();

            $newvendorOtherInformation = new VendorOtherInformation;
            $newvendorOtherInformation->vendor_id =$vendor_id;
            $newvendorOtherInformation->registrant_name = $request->registrant_name ?? 'null';
            $newvendorOtherInformation->registrant_posotion = $request->registrant_posotion ?? 'null' ;
            $newvendorOtherInformation->registrant_contact_number = $request->registrant_contact_number ?? 'null';
            $newvendorOtherInformation->registrant_email = $request->registrant_email ?? 'null';
            $newvendorOtherInformation ->reg_country_code  =   $request->countrycode;
            $newvendorOtherInformation->save();


        } catch (\Throwable $th) {
            return back()->withErrors([
                'invalid' => $th->getMessage(),
            ]);
        }

        return back()->with([
            'flash' => __('We will Review Your Application and contact you  within one working Day.'),
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
