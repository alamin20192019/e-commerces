<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocalizationController extends Controller
{
    public function switch(Request $request, $locale)
    {
        // dd($locale);
        if(session()->has('locale')){
            session()->forget('locale');
        }

        session(['locale' => $locale]);
        return redirect()->back();
    }
}
