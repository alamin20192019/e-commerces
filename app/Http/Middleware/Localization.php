<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $browserLangs = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? parseLanguageList($_SERVER['HTTP_ACCEPT_LANGUAGE']) : ['en'];
        // dd($browserLangs);
        // dump(!session()->has('locale') && (!in_array('zh-hk', $browserLangs) && !in_array('zh-cn', $browserLangs)));

        if(session()->has('locale') && session('locale') === 'js'){
            session()->forget('locale');
        }

        if(auth()->check() && !session()->has('locale')){
            session(['locale' => auth()->user()->locale]);
        }

        if (!session()->has('locale') && (!in_array('zh-hk', $browserLangs) && !in_array('zh-cn', $browserLangs))) {
            session(['locale' => 'en']);
        }
        if (!session()->has('locale') && in_array('zh-cn', $browserLangs)) {
            session(['locale' => 'zh-CN']);
        }

        if (!session()->has('locale') && in_array('zh-hk', $browserLangs)) {
            session(['locale' => 'zh-HK']);
        }
        
        // dump(session()->has('locale'));
        // dd(session('locale'));

        $locale = session()->has('locale') ? session('locale') : config('app.locale');
        // dd($locale);
        App::setLocale($locale);
        
        return $next($request);
    }
}
