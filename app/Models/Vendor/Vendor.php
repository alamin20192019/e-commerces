<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Vendor\VendorInformation;
use App\Models\Vendor\VendorOtherInformation;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;

// use Spatie\Translatable\HasTranslations;
use AhmedAliraqi\LaravelMediaUploader\Entities\Concerns\HasUploader;

class Vendor extends Model implements HasMedia
{

    use HasFactory;
    // use HasTranslations;
    use HasFactory;
    use HasUploader;
    use InteractsWithMedia;


    use HasFactory;
    protected $guarded = [];



    public function vendorInformation()
    {
        return $this->hasone(VendorInformation::class,'vendor_id', 'id');
    }

    public function vendorOtherInformation()
    {
        return $this->hasone(VendorOtherInformation::class,'vendor_id', 'id');
    }

    public function br_file(Media $media = null): void
    {
        // $this->addMediaConversion('br_file')
        //     ->performOnCollections('br_file')
        //     ->fit(Manipulations::FIT_CROP, 300, 300)
        //     ->nonQueued();

        $this->addMediaConversion('br_file')
        ->width(300)
        ->height(300)
        ->performOnCollections('br_file')
        ->fit(Manipulations::FIT_CROP, 300, 300)
        ->nonQueued();
    }

    public function company_photo(Media $media = null): void
    {
        $this->addMediaConversion('componay_photo')
        ->width(300)
        ->height(300)
        ->performOnCollections('componay_photo',300,300 )
        ->fit(Manipulations::FIT_CROP)
        ->nonQueued();
    }

    public function user()
    {
        $this->belongsTo(User::class, 'id', 'user_id');
    }
}
