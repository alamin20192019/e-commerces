<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Vendor\Vendor;
class VendorInformation extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function vandor()
    {
        return $this->hasOne(Vendor::class, 'id', 'vendor_id');
    }
}
