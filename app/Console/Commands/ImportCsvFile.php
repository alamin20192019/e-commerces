<?php

namespace App\Console\Commands;

use App\Models\Brands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ImportCsvFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:csv
                                {file : file name with .csv format}
                                {--target=User : the model target}
                                {--D|debug : activate debug mode}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importing CSV file tool, currently available for Brands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = $this->argument('file');
        $target = $this->option('target');
        // dd($target);
        // dd(Storage::exists('data/'.$file));
        $content = fopen(storage_path('app//data/'.$file), "r");
        $arr = CSVToArray($content);

        // dd($arr);
        switch ($target) {
            case 'Brands':
                $this->InsertToBrands($arr);
                break;
            
            default:
                # code...
                break;
        }
        return 0;
    }

    private function InsertToBrands($arr)
    {
        foreach($arr as $key => $item){
            // dd($item);
            try {
                $newBrand = new Brands;
                $newBrand->name = slugify($item['name'], new Brands, 'name');
                $newBrand->title = [
                    'en' => $item['name'],
                    'zh' => $item['name'],
                ];
                $newBrand->description = null;
                $newBrand->save();

                $this->info('imported data #'.$key.' successfully!');
            } catch (\Throwable $th) {
                $this->error($th->getMessage());
            }
        }
    }
}
