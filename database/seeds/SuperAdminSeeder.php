<?php

namespace Database\Seeders;

use App\Models\Team;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = json_decode(json_encode(explode(',', env('MASTER_TEAM'))));
        // $users = explode(',', env('MASTER_USERS'));
        // var_dump(base_path('users.json'));
        $userstring = file_get_contents(base_path(env('MASTER_USERS')));
        // var_dump($userstring);
        $users = json_decode($userstring);
        // var_dump($users);
        // exit;
        if(count($users) < 1){
            var_dump('Not enough data to continue please check .env.example file to setup users.');
            exit;
        }
        foreach($users as $key => $user){
            try {
                $dev = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'password' => Hash::make(env('MASTER_PASS')),
                    'locale' => $user->locale,
                    'email_verified_at' => Carbon::now()->format("Y-m-d H:i:s"),
                    'roles' => json_encode($user->roles),
                ]);

                if ($key == 0) {
                    // dd($admin);
                    foreach ($teams as $team) {
                        Team::create([
                            'user_id' => $dev->id,
                            'name' => $team,
                            'personal_team' => 1,
                        ]);
                    }
                }else{
                    Team::create([
                        'user_id' => $dev->id,
                        'name' => $user->name,
                        'personal_team' => 1,
                    ]);
                }
            } catch (\Throwable $th) {
                var_dump($th->getMessage()); exit;
            }

            app()->setLocale($user->locale);
            Password::sendResetLink(['email' => $dev->email, 'password' => $dev->password]);
        }

        app()->setLocale(config('app.locale'));
    }
}
